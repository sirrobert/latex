
# Latex (alternately "LaTeX")

This project fork arises from a minor quibble with the excellent LaTeX
project.  The particular details are unimportant, but the result is this
repository.  

Through herculean effort, the project has been meticulously "repaired", if
I may be so bold, and rendered infinitely more usable.  Full details may be
found in the commit log.

## Thanks

An enormous debt of gratitude is owed (and fully repaid in this sentence) to
Mr. Leslie Lamport, originator of the parent project upon which this fork is
based.

## Changelog

* Replaced all pronunciations of "latex" with the repo-standard "latex"
(homophonous with the name of the rubber substance).





